require 'rexml/document'
require 'rexml/xpath'
require 'logger'
require 'socket'
require 'thread'
require 'singleton'

$imds = []

###
# Monolithic modules
###

###
#	Rofl Logger Module
###
#check if there already is a logger, kind of a constructor
def rofl_logger_check
	@debugname,@logger = nil,nil
  if @logger.nil?
    #to stop output from getting messy, we use a logger
    @logger = Logger.new(STDOUT)
    @logger.level = Logger::DEBUG
    #@logger.datetime_format = "%Y-%m-%d %H:%M:%S" #useful for logging to a file
    @logger.datetime_format = "%H:%M:%S" #useful for debugging
    @debugname = self.class if @debugname.nil? #only used to inform the user
    @tracing = false
    #enable tracing
    if @tracing
      rofl_enable_silent_trace
    end
  end
end

#error message
def elog text="error" 
  rofl_logger_check #check if logger is setup
  @logger.error "#{@debugname}.#{rofl_meth_trace.to_s}: #{text.to_s}"
end

#warning
def wlog text="warning"
  rofl_logger_check #check if logger is setup
  @logger.warn "#{@debugname}.#{rofl_meth_trace.to_s}: #{text.to_s}"
end

#info message
def ilog text="info"
  rofl_logger_check #check if logger is setup
  @logger.info "#{@debugname}.#{rofl_meth_trace.to_s}: #{text.to_s}"
end

#debug message
def dlog text="debug"
  rofl_logger_check #check if logger is setup
  @logger.debug "#{@debugname}.#{rofl_meth_trace.to_s}: #{text.to_s}"
end

#get method call trace
def rofl_meth_trace
  last_meth_name = "notrace"
  skip = 2 #indicates how many items we skip in the execution stack trace
  call_trace = caller(skip)
  regexp = /\`.*?\'/
  last_meth = call_trace[0][regexp]
  last_meth_name = last_meth.delete("\`") unless last_meth.nil?
  last_meth_name = last_meth_name.delete("\'") unless last_meth_name.nil?
  return last_meth_name
end
###
#	Rofl Logger Module - END
###

###########

# The interesting processing code starts here

###########

# image description class, created from an imd_node, forms a tree
class IMD
  attr_accessor :parent, :childs
  attr_accessor :description # verbal description
  attr_accessor :transform # matrix 4x4
  attr_accessor :size      # {:x,:y,:z}
  attr_accessor :color     # {:r,:g,:b}
  attr_accessor :special   # special? shall we ignore it later on, when removing parts of the tree
  attr_accessor :is_visible   # boolean with value, indicating visibility

  def initialize imd_node, parent
    @parent = parent
    @color = next_color
    @description = REXML::XPath.first(imd_node , "VerbalDescriptions/VDescription").text
    @special = is_special?
    @is_visible = false
    rows = []
    (1..4).each do |i| 
      rows << REXML::XPath.first(imd_node , "TransMat/row#{i}").text.split(" ").map { |s| s.to_f }
    end    
    @transform = [rows[0], rows[1], rows[2], rows[3]]
    @size = { :x=>0, :y=>0, :z=>0 }
    REXML::XPath.each(imd_node, "ObjectSchemes/Scheme/Axis[@label]") do |axis|
      case (axis.attributes["label"])
      when "x" then @size[:x] = axis.attributes["abs_deg"].to_f
      when "y" then @size[:y] = axis.attributes["abs_deg"].to_f
      when "z" then @size[:z] = axis.attributes["abs_deg"].to_f
      end
    end
    @size = { :x=>0, :y=>0, :z=>0 } if @special
    @childs = []
    REXML::XPath.each(imd_node, "IMD") { |node| @childs << IMD.new(node, self) }
  end
  
  #is this one special?
  def is_special?
    #the IDT bounding boxes are treated specially
    specials = ["lm2_rathaus","lm3_kirchplatz","lm3_rtrundturmkirche","lm3_dtdoppelturmkirche","lm4_kapelle","lm4_gruendstueck"]
    #and even more, stuff we just want to skip
    ["Strasse","bruecke","LM5_korpusbrunnen","LM5_kreisverkehr","LM5_brunnen","LM5_sockel"].each {|i| specials << i}
    return specials.include? @description
  end
   
  #get next available color
  def next_color
    last_r,last_g,last_b = $colors[:last][:r],$colors[:last][:g],$colors[:last][:b]
    color = { :r=>last_r, :g=>last_g, :b=>last_b }    
    #count down the colors, if there's a slimmer way to do this, let me know.
    color[:b] -= 10 if last_b > 10; color[:b] = 255 if last_b < 10
    color[:g] -= 10 if (last_b < 10 && last_g > 10); color[:g] = 255 if last_g < 10
    color[:r] -= 10 if (last_r < 10 && last_b > 10); color[:r] = 255 if last_r < 10
    #set the last used color
    $colors[:last] = color
    #we store the IDT for later computation
    key = 1000000000+(color[:r]*1000000)+(color[:g]*1000)+color[:b]
    $colors[key] = self #now, we can easily access the IDT by their colors
    return color
  end
end

###
# processing programm logics
###

#method that (re)initializes all variables needed to run
def reinit
  $colors = { :last=>{ :r=>255, :g=>255, :b=>255 } }
  $do_screenshot = false
  $do_color_compute = true
  $imds = read_xml "baseIMD.xml"
  puts $imds.inspect
end

#setup
def setup
  size 1000, 700, P3D
  frameRate 2
  no_stroke
  rofl_logger_check
  #dbus_connect
  reinit
end

#read the xml_file
def read_xml filename
    xml = REXML::Document.new(File.new filename)
    imds = []
    REXML::XPath.each(xml, "/IMD") { |imd_node| imds << IMD.new(imd_node, nil) }
    return imds  
end

#The processing drawing is starting from here:

#the draw method
def draw
  background 000
  #camera(eyeX, eyeY, eyeZ, centerX, centerY, centerZ, upX, upY, upZ)
  #default values for camera(width/2.0, height/2.0, (height/2.0) / tan(PI*60.0 / 360.0), width/2.0, height/2.0, 0, 0, 1, 0)
  camera() #default camera values
  #ok, the factor before the z component should be 1
  translate width/2.0, height/2.0, 0.75*(height.to_f/2.0 / tan(PI*60.0 / 360.0))  
  
  #ok, now draw IMDs
  rotate_x PI/2
  if $startnode
  	dlog "will now try to draw with startnode: #{$startnode}"
  	draw_imd_recursive $startnode
  else
  	draw_imd_recursive
  end
  
  #make a screenshot, if we're told to do it
  make_screenshot if $do_screenshot
    
  #compute the colors
  compute_colors if $do_color_compute
  
  # read from stdin
  parse_stdin  
end

# stdin parser
def parse_stdin
	dlog "Waiting for input:"
  s = gets.strip
  dlog "got input: #{s}"
  if s.include? "show_colors"
  	compute_colors
  	show_colors
  end
  if s.include? "startnode="
  	$startnode = s.split("startnode=")[1]
		dlog "set startnode to: #{$startnode}"
  end
  if s.include? "resetstartnode"
  	$startnode = nil
  end 
end

# output color information to console
def show_colors
  output = []
  $visible_colors.each do |color,surface|
    imd = $colors[color]
    unless imd.nil?
      imd.is_visible = true
      output << imd.description 
    end
  end
  puts output.join(",")
end

#make a screenshot
def make_screenshot
  #reset variable
  $do_screenshot = false
  saveFrame("output/renderedscene.png")
end

#compute the colors in the image
def compute_colors
  #reset variable
  $do_color_compute = false
  ilog "Starting to compute colors."
  #load the pixels from the rendered scene, will be stored into pixels[]
  load_pixels
  #we will store this in a hash, OMG :) (~700000 pixels and 1000000000+ colors)
  $visible_colors = {}
  started_at = Time.now #profiling timer
  #now find out, which colors are used
  pixels.each do |i| 
    #this returns color surface amounts on pixel basis
    key = 1000000000+(red(i).to_i*1000000)+(green(i).to_i*1000)+blue(i).to_i
    has_key = $visible_colors.has_key? key
    $visible_colors[key] += 1 if has_key
    $visible_colors[key] = 1 unless has_key
    #this will only inform you, if a color is used in the picture
    #icolors[1000000000+(red(i).to_i*1000000)+(green(i).to_i*1000)+blue(i).to_i] = 1
  end
  ilog "Found #{$visible_colors.keys.length} different colors in the picture."
  puts "DEBUG: Took me #{Time.now - started_at} seconds."
  flag_visible_nodes
  #apply dirty hack
  solve_parental_issues
end

#method to flag visible nodes
def flag_visible_nodes
  $visible_colors.each do |color,surface|
    imd = $colors[color]
    unless imd.nil?
      imd.is_visible = true
      #puts "DEBUG: IMD: #{imd.description} with color #{color} covers a 2D-surface of #{surface}" 
    end
  end
end

#HACK: flag parents of visible nodes visible
#this is not correct, but must be done because gesImagery would break otherwise
def solve_parental_issues
  $imds.each do |imd|
    parent_hack imd unless imd.parent.nil?   
  end
end

#make the parent visible
def parent_hack imd
  unless imd.parent.nil? 
    imd.parent.is_visible = true
    parent_hack imd.parent
  end
end

#debug method showing the invisible nodes
def show_invisible_nodes
  $imds.each do |imd|
    unless imd.is_visible
      puts "DEBUG: IMD: #{imd.description} is invisible." 
      #check for visible childs of an invisible node
      find_visible_children imd 
    end
  end
end

#check for visible childs of an invisible node
def find_visible_children imd
  if imd.is_visible
    puts "DEBUG: FOUND: IMD: #{imd.description} is visible."
    puts "DEBUG: FOUND: With parent: #{imd.parent.description} is invisible!"
  end
  imd.childs.each { |child| find_visible_children child }
end
 

#draw the imds recursively
def draw_imd_recursive startnode=nil
	if startnode
		imd = get_imd($imds.first,startnode)
	  #dlog "found imd: #{imd.inspect}"
	  #draw_imd imd
	else
	  $imds.each do |imd| 
	    draw_imd imd
	  end
	end
end

def get_imd imd,description
	imd.childs.each do |child|
		#child.childs.each {|c| puts "child: #{child.description} grand child: #{c.description}"}
		if child.description.eql? description
			child.childs.each {|c| puts "child: #{child.description} grand child: #{c.description}"}
			#dlog "child description: #{child.inspect}"
			draw_imd child
			#return child
		else
			get_imd child,description
		end
	end
end

#draw imd and call draw_imd on childs
def draw_imd imd
  pushMatrix
  begin
    fill imd.color[:r], imd.color[:g], imd.color[:b]
  rescue
    puts "ERROR: Color fill failed. Inspect color: #{imd.color.inspect}"
    fill 255, 255, 255
  end
  t = imd.transform
  #as you can see, LOL, we need to transpose the matrix, it's an AVANGO thingy
  applyMatrix(t[0][0],t[1][0],t[2][0],t[3][0],
              t[0][1],t[1][1],t[2][1],t[3][1],
              t[0][2],t[1][2],t[2][2],t[3][2],
              t[0][3],t[1][3],t[2][3],t[3][3])
  size_correction = 1     
  box imd.size[:x]*size_correction, imd.size[:y]*size_correction, imd.size[:z]*size_correction
  imd.childs.each { |child| draw_imd child }  
  popMatrix
end
