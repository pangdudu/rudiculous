require 'rubygems'
require 'dbus_client'
require 'rofl'
include DBusClient

socket_name = "tcp:host=127.0.0.1,port=2687,family=ipv4"
service_name = "org.rudiculous.Service"
default_iface = "org.rudiculous.Service.CullyInterface"
object_path = "/org/rudiculous/Service/Cully"

#connect to bus
dbus = DBus::RemoteBus.new(socket_name)
#get proxy object
service = dbus.service(service_name)
cully = service.object(object_path)
cully.introspect
cully.default_iface = default_iface
#call a method on cully
cully.change_pov "push it real good!"

